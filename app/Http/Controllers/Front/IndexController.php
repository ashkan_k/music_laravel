<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Track;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        $tracks = Track::all();
        return view('front.index', compact('tracks'));
    }
}

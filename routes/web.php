<?php

use App\Http\Controllers\Front\IndexController;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//
////    dd(\Illuminate\Support\Facades\DB::select('describe singers'));
//
////    User::truncate();
////    $user = User::create(
////        [
////            'first_name' => 'اشکان',
////            'last_name' => 'کریمی',
////            'email' => 'as@gmail.com',
////            'phone' => '09396988720',
////            'password' => Hash::make('123'),
////            'email_verified_at' => Carbon::now(),
////            'is_superuser' => true,
////        ]
////    );
////    auth('api')->login($user);
//
//    return view('front.index');
//});
use Laravel\Socialite\Facades\Socialite;

Route::get('/auth/google', function () {
    return Socialite::driver('google')->redirect();
});


Route::get('/auth/google/callback', function () {
    $googleUser = Socialite::driver('google')->user();

    $user = User::updateOrCreate([
        'google_id' => $googleUser->id,
    ], [
        'first_name' => $googleUser->name,
        'last_name' => $googleUser->name,
        'email' => $googleUser->email,
        'password' => Hash::make($googleUser->token),
    ]);

    $credentials = ['email' => $googleUser->email, 'password' => $googleUser->token];

    if (! $token = auth()->setTTL(10080)->attempt($credentials)) {
        return response()->json(['error' => 'Unauthorized'], 401);
    }

    auth()->login($user);

    return $token;
});

Auth::routes();

Route::get('/', [IndexController::class, 'index'])->name('index');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
